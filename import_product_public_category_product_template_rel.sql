CREATE TABLE product_public_category_product_template_rel(
   product_public_category_id INTEGER  NOT NULL PRIMARY KEY 
  ,product_template_id        INTEGER  NOT NULL
);
INSERT INTO product_public_category_product_template_rel(product_public_category_id,product_template_id) VALUES (54,7);
INSERT INTO product_public_category_product_template_rel(product_public_category_id,product_template_id) VALUES (53,8);
INSERT INTO product_public_category_product_template_rel(product_public_category_id,product_template_id) VALUES (54,9);
INSERT INTO product_public_category_product_template_rel(product_public_category_id,product_template_id) VALUES (54,10);
