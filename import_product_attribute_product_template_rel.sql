CREATE TABLE product_attribute_product_template_rel(
   attribute_id    INTEGER  NOT NULL PRIMARY KEY 
  ,product_tmpl_id INTEGER  NOT NULL
);
INSERT INTO product_attribute_product_template_rel(attribute_id,product_tmpl_id) VALUES (1,7);
INSERT INTO product_attribute_product_template_rel(attribute_id,product_tmpl_id) VALUES (3,7);
INSERT INTO product_attribute_product_template_rel(attribute_id,product_tmpl_id) VALUES (1,8);
INSERT INTO product_attribute_product_template_rel(attribute_id,product_tmpl_id) VALUES (3,8);
INSERT INTO product_attribute_product_template_rel(attribute_id,product_tmpl_id) VALUES (1,9);
INSERT INTO product_attribute_product_template_rel(attribute_id,product_tmpl_id) VALUES (3,9);
INSERT INTO product_attribute_product_template_rel(attribute_id,product_tmpl_id) VALUES (1,10);
INSERT INTO product_attribute_product_template_rel(attribute_id,product_tmpl_id) VALUES (3,10);
