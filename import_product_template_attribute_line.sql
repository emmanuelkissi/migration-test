CREATE TABLE product_template_attribute_line(
   attribute_id    INTEGER  NOT NULL PRIMARY KEY 
  ,product_tmpl_id INTEGER  NOT NULL
  ,active          VARCHAR(4) NOT NULL
  ,create_uid      INTEGER  NOT NULL
  ,create_date     VARCHAR(26) NOT NULL
  ,write_uid       INTEGER  NOT NULL
  ,write_date      VARCHAR(26) NOT NULL
);
INSERT INTO product_template_attribute_line(attribute_id,product_tmpl_id,active,create_uid,create_date,write_uid,write_date) VALUES (1,7,'TRUE',2,'2021-09-24 18:59:50.479620',2,'2021-09-24 18:59:50.479636');
INSERT INTO product_template_attribute_line(attribute_id,product_tmpl_id,active,create_uid,create_date,write_uid,write_date) VALUES (3,7,'TRUE',2,'2021-09-24 18:59:50.479732',2,'2021-09-24 18:59:50.479738');
INSERT INTO product_template_attribute_line(attribute_id,product_tmpl_id,active,create_uid,create_date,write_uid,write_date) VALUES (1,8,'TRUE',2,'2021-09-24 18:59:50.479857',2,'2021-09-24 18:59:50.479863');
INSERT INTO product_template_attribute_line(attribute_id,product_tmpl_id,active,create_uid,create_date,write_uid,write_date) VALUES (3,8,'TRUE',2,'2021-09-24 18:59:50.479946',2,'2021-09-24 18:59:50.479951');
INSERT INTO product_template_attribute_line(attribute_id,product_tmpl_id,active,create_uid,create_date,write_uid,write_date) VALUES (1,9,'TRUE',2,'2021-09-24 18:59:50.480036',2,'2021-09-24 18:59:50.480041');
INSERT INTO product_template_attribute_line(attribute_id,product_tmpl_id,active,create_uid,create_date,write_uid,write_date) VALUES (3,9,'TRUE',2,'2021-09-24 18:59:50.480122',2,'2021-09-24 18:59:50.480127');
INSERT INTO product_template_attribute_line(attribute_id,product_tmpl_id,active,create_uid,create_date,write_uid,write_date) VALUES (1,10,'TRUE',2,'2021-09-24 18:59:50.480208',2,'2021-09-24 18:59:50.480213');
INSERT INTO product_template_attribute_line(attribute_id,product_tmpl_id,active,create_uid,create_date,write_uid,write_date) VALUES (3,10,'TRUE',2,'2021-09-24 18:59:50.480295',2,'2021-09-24 18:59:50.480300');
