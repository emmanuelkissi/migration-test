CREATE TABLE stock_quant(
   location_id       INTEGER  NOT NULL PRIMARY KEY 
  ,quantity          INTEGER  NOT NULL
  ,reserved_quantity BIT  NOT NULL
  ,company_id        BIT  NOT NULL
  ,product_id        INTEGER  NOT NULL
);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,8);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,9);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,10);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,11);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,12);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,5,0,1,13);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,5,0,1,14);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,3,0,1,15);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,5,0,1,16);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,3,0,1,17);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,18);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,19);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,20);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,21);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,22);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,33);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,34);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,35);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,36);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,37);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,38);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,39);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,40);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,41);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,42);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,43);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,44);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,45);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,46);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,47);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,48);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,49);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,51);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,53);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,54);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,55);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,56);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,58);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,59);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,63);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,64);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,65);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,66);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,67);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,69);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,71);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,2,0,1,73);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,75);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (45,1,0,1,76);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (62,1,0,1,24);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (62,2,0,1,25);
INSERT INTO stock_quant(location_id,quantity,reserved_quantity,company_id,product_id) VALUES (62,1,0,1,26);
